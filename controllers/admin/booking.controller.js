const models = require('../../models/index');
const { Validator } = require('node-input-validator');
const booking = require('../../models/booking');
const BOOKING = models.booking;
const PUMP = models.pump;
const USERS = models.users;

//relations
BOOKING.belongsTo(USERS, {
    foreignKey: "user_id"
  });

module.exports = {
    get: async function(req, res, next){

        return res.render('admin/pump/booking');
    },
    search: async function(req, res, next){

        let body = req.body;

        const searchStation = await models.sequelize.query('SELECT p.id,p.title,p.address,( 3959 * acos ( cos ( radians('+body.lat+') ) * cos( radians( p.lat ) ) * cos( radians( p.lng ) - radians('+body.lng+') ) + sin ( radians('+body.lat+') ) * sin( radians( p.lat ) ) ) ) AS `distance` from pump p WHERE p.deletedAt IS NULL HAVING distance < 100 ORDER BY distance LIMIT 0 , 20', {
            model: PUMP,
            mapToModel: true // pass true here if you have any mapped fields
        });

        if(searchStation){

            return res.json({
                status: 200,
                message: 'Search Results Found',
                data: searchStation
            });

        }else{
            return res.json({
                status: 201,
                message: 'No Search Results Found',
                data: null
            });
        }

    },
    add: async function(req, res, next){

        let pump_id = req.params.pump_id;
        let user_id = req.session.id; //session id

        const pumps = await PUMP.findOne({
            where: {
                deletedAt: null,
                id: pump_id
            }
        });


        return res.render('admin/pump/newbooking',{pump_id,user_id,pumps});
    },
    store: async function(req, res, next){

        try{

            let body = req.body;

            let bookingValid =   new Validator(body,{
                title:              'required|maxLength:150',
                bookingdate:           'required',
                vehicles:               'required',
                location:               'required',
            });
        
        
            let booking = await bookingValid.check();

            if(!booking){
                return res.status(201).json({status:201, message:bookingValid.errors});
            }

            const insertBooking = await BOOKING.create({
                    pump_id:        body.pump_id,
                    title:          body.title,
                    user_id:        body.user_id,
                    location:       body.location,
                    lat:            body.lat,
                    lng:            body.lng,
                    vehicles:       body.vehicles,
                    bookingdate:    body.bookingdate,
                    comments:       body.comments 
            });

            if(insertBooking){
                return res.status(200).json({
                    status: 200,
                    message: 'New booking added successfully!'
                });
            }else{
                return res.status(204).json({
                    status: 204,
                    message: 'Error Occured! Please try again.'
                });
            }

        }catch(e){
            return res.status(422).json({
                status: 422,
                message: 'Something Went Wrong!'
            });
        }
    },
    destroy: async function(req, res, next){
        
    try{

        let id = req.params.id;

        const selectPump = await PUMP.findOne({
            where: {id: id}
        });

        if(selectPump){
                
                const destroyPump = await PUMP.destroy({
                    where: {id: id}
                });

                if(destroyPump){
                    return res.status(200).json({
                        status: 200,
                        message: 'Gas station deleted successfully!'
                    });
                }else{
                    return res.status(204).json({
                        status: 204,
                        message: 'Error Occured! Please try again.'
                    });
                }
        }else{
            return res.status(204).json({
                status: 204,
                message: 'Invalid Gas Station.'
            });
        }

    }catch(e){
        return res.json({status: 400, message: e});
    }

    },
    viewbooking: async function(req, res, next){

        const bookings = await BOOKING.findAll({
            include: [
                {
                    model: USERS,
                    attributes: ['id','firstName','lastName','email']

                }
            ]
        });

        return res.render('admin/pump/viewbooking',{bookings});
    },
    block: async function(req, res, next){
        const status = req.params.status;
        const id = req.params.id;

        const blockBooking = await BOOKING.update(
            {status: status},
            {where: { id: id} }
        );

        if(blockBooking){
            return res.status(200).json({
                status: 200,
                message: 'Booking Status updated!'
            });
        }else{
            return res.status(204).json({
                status: 204,
                message: 'Error Occured! Please try again.'
            });
        }
    }
}