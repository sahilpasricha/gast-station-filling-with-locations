//All Configurations Come From Here
require('dotenv').config({
    path: `./env-files/${process.env.NODE_ENV || 'development'}.env`,
  })

const { Validator } = require('node-input-validator');
const bcrypt = require('bcrypt');
const jwt = require("jsonwebtoken");
const jwtKey = process.env.JWT_SECRET;
const jwtExpirySeconds = 300;

const models = require('../../models/index');
const { use } = require('../../routes/admin');
const Users = models.users;

module.exports = {
    login: async function(req,res){

        try{
            let formData = req.body;
            const validate = await new Validator(formData,{
                email: 'required',
                password: 'required'
            });
            let matched = await validate.check();
            if(!matched){
                return res.status(500).json({message: "email or password requird!"})
            } else{
                var checkEmail = await Users.findOne({ where: { email: formData.email  }});
                //Check if email is valid
                if(checkEmail){
                    var checkPassword = await bcrypt.compare(formData.password, checkEmail.dataValues.password);
                    //then check if password is valid
                    let uid = checkEmail.id;
                    
                    if(checkPassword){
                      
                    //1 hr Session Time
                    var hour = 365 * 365 * 24 * 60 * 60 * 1000;
                    // //Set Session Time 1 HR
                    // Store Session

                    const token = jwt.sign({ uid }, jwtKey, {
                        algorithm: "HS256",
                        expiresIn: jwtExpirySeconds,
                    });

                    
                    await Users.update(
                        {sessionToken: token},
                        {where: {id: uid} }
                    );

                    req.session.email = checkEmail.email; 
                    req.session.id = checkEmail.id;

                    res.cookie("token", token, { maxAge: jwtExpirySeconds * 1000 });

                    return res.json({status:200, message:'Logged In Successfully!'});
                        //redirect
                    }else{
                        return res.json({status:201, message:'Invalid Email/Password'});
                    }
                }else{
                    //if not valid  action??
                    return res.json({status:201, message:'Invalid Email/Password'});
                }
            }
            }catch(e){
                return console.log(e);
            }


    },
    loginget: async function(req,res){  

        if("email" in req.session && req.session.email != ""){
            return res.redirect('/admin');
        }
        return res.render('admin/auth/login');

    },
    logout: async function(req, res){
        req.session.destroy();
        return res.redirect('/admin/login');
    }
}