const models = require('../../models/index');
const Users = models.users;

module.exports = {
    /* Starting with basic CRUD Resources */
    get: async function(req,res){
        console.log(req.headers.host);
        const users = await Users.findAll({  order: [['createdAt', 'DESC']] });
        return res.render('admin/user',{users});
    },
    delete: async function(req,res){
        
        let id = req.params.id;

        await Users.destroy({
            where: {
                id: id
            }
        });

        return res.redirect('../../users');

    },
    edit: async function(req,res){

        try{

        let id = req.params.id;
        let body = req.body;

        const updateUser = await Users.update(
            {
                firstName: body.firstname,
                lastName: body.lastName,
                email: body.email,
                phoneNumber: body.phone,
                status: body.status
            },
            { where: { id: id } }
        );

        return res.json( {status:200,message:"User Updated!"} );

        }catch(e){
            return res.json({status: 400, message: e});
        }

    }
}