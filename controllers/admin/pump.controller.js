const models = require('../../models/index');
const { Validator } = require('node-input-validator');
const PUMP = models.pump;
const maxFileSize = parseInt(3 * 1024 * 1024);
const allowedFile = "image";

module.exports = {
    pump: async function(req, res, next){

        const pumps = await PUMP.findAll({
            where: {
                deletedAt: null
            },
            order: [
                ['createdAt', 'DESC']
            ] 
    });

        return res.render('admin/pump/addpump',{pumps});
    },
    store: async function(req, res, next){

        try{

            let body = req.body;

            let pumpValid = await new Validator(body,{
                title:              'required|maxLength:70',
                location:           'required',
                lat:                'required',
                lng:               'required'
            });
        
        
            let pump = await pumpValid.check();

            if(!pump){
                return res.status(201).json({status:201, message:pumpValid.errors});
            }

            let imageName = "";

            if(req.files != null){

                if("image" in req.files){

                    let imageFile = req.files.image;
                    let filetype = imageFile.mimetype;
                    let mimetype = filetype.split('/');

                    if(imageFile.size <= maxFileSize){

                        if(mimetype[0] == allowedFile){

                            imageName = Date.now()+imageFile.name;

                            imageFile.mv('./public/images/'+ imageName, function(err) {
                                if (err)
                                    console.log(err);  
                            });

                        }else{
                            res.json({
                                status: 201,
                                message: 'Only Image file is allowed!'
                            });
                        }

                    }else{
                        res.json({
                            status: 201,
                            message: 'Max File Size Allowed is 3 MB'
                        });
                    }

                }

            }

            const insertPump = await PUMP.create({
                    title:      body.title,
                    address:    body.location,
                    lat:        body.lat,
                    lng:        body.lng,
                    image:      imageName
            });

            if(insertPump){
                return res.status(200).json({
                    status: 200,
                    message: 'New Gas station added successfully!'
                });
            }else{
                return res.status(204).json({
                    status: 204,
                    message: 'Error Occured! Please try again.'
                });
            }

        }catch(e){
            return res.status(422).json({
                status: 422,
                message: 'Something Went Wrong!'
            });
        }
    },
    destroy: async function(req, res, next){
        
    try{

        let id = req.params.id;

        const selectPump = await PUMP.findOne({
            where: {id: id}
        });

        if(selectPump){
                
                const destroyPump = await PUMP.destroy({
                    where: {id: id}
                });

                if(destroyPump){
                    return res.status(200).json({
                        status: 200,
                        message: 'Gas station deleted successfully!'
                    });
                }else{
                    return res.status(204).json({
                        status: 204,
                        message: 'Error Occured! Please try again.'
                    });
                }
        }else{
            return res.status(204).json({
                status: 204,
                message: 'Invalid Gas Station.'
            });
        }

    }catch(e){
        return res.json({status: 400, message: e});
    }

    } 
}