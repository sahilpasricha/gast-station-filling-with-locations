var express = require('express');
var router = express.Router();

const mountAuthRoutes =     require('./auth');
const mountPumpRoutes = require('./pump');
const mountBookingRoutes = require('./booking');

function auth(req, res, next){
  console.log("comere herw wqith",req.session.email);
  if("email" in req.session && req.session.email != ""){
    
      return next();

  }
  return res.redirect('/admin/login');
}


router.get('/', auth, function(req, res, next) {
    return res.redirect('/admin/booking/view');
});


//Moutable Routes
mountAuthRoutes(router,auth);
mountPumpRoutes(router,auth);
mountBookingRoutes(router,auth);

module.exports = router;