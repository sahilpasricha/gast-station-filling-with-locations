const pumpController = require('../../controllers/admin/pump.controller');

module.exports = (router,auth) => {

    router.get('/pump',auth,pumpController.pump);

    router.post('/pump',auth,pumpController.store);
    router.delete('/pump/:id',auth,pumpController.destroy);

    return router;
};