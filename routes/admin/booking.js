const bookingController = require('../../controllers/admin/booking.controller');
const booking = require('../../models/booking');

module.exports = (router,auth) => {

    router.get('/booking',auth,bookingController.get);
    router.post('/booking/search',auth,bookingController.search);
    router.get('/booking/add/:pump_id',auth,bookingController.add);
    router.post('/booking',auth,bookingController.store);
    router.get('/booking/view',auth,bookingController.viewbooking);
    router.get('/booking/block/:id/:status',auth,bookingController.block);
    router.delete('/booking/:id',auth,bookingController.destroy);

    return router;
};