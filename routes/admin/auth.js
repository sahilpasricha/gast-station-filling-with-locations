//All Configurations Come From Here
require('dotenv').config({
  path: `./env-files/${process.env.NODE_ENV || 'development'}.env`,
})

var express = require('express');
var router = express.Router();
const authCtrl = require('../../controllers/admin/auth.controller');
const jwt = require("jsonwebtoken");
const jwtKey = process.env.JWT_SECRET;

module.exports = router => {
    router.post('/login', authCtrl.login);
    router.get('/login',authCtrl.loginget);
    router.get('/logout',authCtrl.logout);
    return router;
  };