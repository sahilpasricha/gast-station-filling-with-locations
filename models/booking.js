/* jshint indent: 2 */
const moment = require('moment');

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('booking', {
    id: {
      type: DataTypes.UUID,
      allowNull: false,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4
    },
    pump_id: {
      type: DataTypes.INTEGER,
      references: 'pump',
      referencesKey: 'id',
      allowNull: false
      
    },
    title: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: "Untitled"
    },
    user_id: {
      type: DataTypes.STRING(150),
      references: 'users',
      referencesKey: 'id',
      allowNull: false,
      defaultValue: ""
    },
    location: {
      type: DataTypes.STRING(150),
      allowNull: true,
      defaultValue: ""
    },
    lat: {
      type: DataTypes.STRING(150),
      allowNull: true,
      defaultValue: ""
    },
    lng: {
      type: DataTypes.STRING(150),
      allowNull: true,
      defaultValue: ""
    },
    vehicles: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaultValue: 1
    },
    bookingdate: {
      type: DataTypes.STRING(30),
      allowNull: false,
      defaultValue: null
    },
    comments: {
      type: DataTypes.STRING(255),
      allowNull: true,
      defaultValue: ""
    },
    status: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaultValue: 1
    }
  }, {
    tableName: 'booking',
    paranoid: true,
    timestamps: true
  });
};
