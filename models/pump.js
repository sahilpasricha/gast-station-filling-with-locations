/* jshint indent: 2 */
const moment = require('moment');

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('pump', {
    id: {
      type: DataTypes.UUID,
      allowNull: false,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4
    },
    title: {
      type: DataTypes.TEXT,
      allowNull: false
      
    },
    address: {
      type: DataTypes.TEXT,
      allowNull: false,
      defaultValue: ""
    },
    lat: {
      type: DataTypes.STRING(100),
      allowNull: true,
      defaultValue: ''

    },
    lng: {
      type: DataTypes.STRING(30),
      allowNull: true,
      defaultValue: ""
    },
    image: {
      type: DataTypes.TEXT,
      allowNull: false,
      defaultValue: ""
    }
  }, {
    tableName: 'pump',
    paranoid: true,
    timestamps: true
  });
};
