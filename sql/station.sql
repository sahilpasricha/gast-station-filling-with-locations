-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 07, 2020 at 04:58 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.3.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `station`
--

-- --------------------------------------------------------

--
-- Table structure for table `booking`
--

CREATE TABLE `booking` (
  `id` int(11) NOT NULL,
  `pump_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL,
  `lat` varchar(255) NOT NULL,
  `lng` varchar(255) NOT NULL,
  `vehicles` int(11) DEFAULT NULL,
  `bookingdate` varchar(255) DEFAULT NULL,
  `comments` varchar(255) NOT NULL,
  `status` int(11) DEFAULT 1,
  `createdAt` timestamp NULL DEFAULT NULL,
  `updatedAt` timestamp NULL DEFAULT NULL,
  `deletedAt` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `booking`
--

INSERT INTO `booking` (`id`, `pump_id`, `title`, `user_id`, `location`, `lat`, `lng`, `vehicles`, `bookingdate`, `comments`, `status`, `createdAt`, `updatedAt`, `deletedAt`) VALUES
(3, 34602367, 'Sahil\'s Iphone', 'Ym44vm4D8ufRGFdYcrExD24S-iNn8lX0', 'Phase 7, Sector 61, Sahibzada Ajit Singh Nagar, Chandigarh, India', '30.70401859999999', '76.7296396', 3, '2020-09-19', 'ddddddddddd', 1, '2020-09-07 14:14:20', '2020-09-07 14:14:20', NULL),
(88, 34602365, 'Sahil\'s Iphone', 'bf4a262b-22b2-4363-95d8-5d1fc7c3748a', 'Islamabad, Pakistan', '33.6844202', '73.04788479999999', 3, '2020-09-11', 'SSSSSSSSS', 0, '2020-09-07 05:49:20', '2020-09-07 06:47:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pump`
--

CREATE TABLE `pump` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `lat` varchar(255) NOT NULL,
  `lng` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `createdAt` timestamp NULL DEFAULT NULL,
  `updatedAt` timestamp NULL DEFAULT NULL,
  `deletedAt` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pump`
--

INSERT INTO `pump` (`id`, `title`, `address`, `lat`, `lng`, `image`, `createdAt`, `updatedAt`, `deletedAt`) VALUES
(1, 'Dell Inspiron 15 Gaming Laptop: Core i7-7700HQ', 'Khanna Pul, Khanna Burma Town, Islamabad, Pakistan', '33.6291327', '73.11355549999999', '1599148495144image.jpg', '2020-09-03 15:54:55', '2020-09-03 15:54:55', NULL),
(2, 'Dell Inspiron 15 Gaming Laptop: Core i7-7700H', 'Trapani, Province of Trapani, Italy', '38.01735050000001', '12.5365171', '1599148462932image.jpg', '2020-09-03 15:54:22', '2020-09-03 15:54:22', NULL),
(97, 'Dell Inspiron 15 Gaming Laptop: Core i7-7700H', 'Trapani, Province of Trapani, Italy', '38.01735050000001', '12.5365171', '1599148188488image002.jpg', '2020-09-03 15:49:48', '2020-09-03 15:49:48', NULL),
(9810, 'Dell Inspiron 15 Gaming Laptop: Core i7-7', 'Erskineville NSW, Australia', '-33.90476', '151.18479', '', '2020-09-03 16:13:38', '2020-09-03 16:13:38', '2020-09-03 16:26:39'),
(34602360, 'Dell Inspiron 15 Gaming Laptop:', 'Khanna, Punjab, India', '30.70707689999999', '76.2169907', '1599146501723fleet-management.png', '2020-09-03 15:21:41', '2020-09-03 15:21:41', '2020-09-03 15:21:41'),
(34602361, 'Dell Inspiron 15 Gaming Laptop:', 'Khanna, Punjab, India', '30.70707689999999', '76.2169907', '1599146674096fleet-management.png', '2020-09-03 15:24:34', '2020-09-03 15:24:34', '2020-09-03 16:30:58'),
(34602362, 'Dell Inspiron 15 Gaming Laptop: Core i7-7700HQ,', 'Ethiopia', '9.145000000000001', '40.489673', '', '2020-09-03 16:08:36', '2020-09-03 16:08:36', NULL),
(34602363, 'Dell Inspiron 15 Gaming Laptop: Core i7-7700HQ,', 'Ethiopia', '9.145000000000001', '40.489673', '1599149331137image.jpg', '2020-09-03 16:08:51', '2020-09-03 16:08:51', NULL),
(34602364, 'Sahil\'s Iphone', 'Chandigarh, India', '30.7333148', '76.7794179', '1599150808788image002.jpg', '2020-09-03 16:33:28', '2020-09-03 16:33:28', '2020-09-03 16:33:39'),
(34602365, 'Cash and Carry', 'Madina Cash & Carry Mall(MCC), Rohtas Road, G-9 Markaz G 9 Markaz G-9, Islamabad, Pakistan', '33.6900626', '73.0308384', '1599366083485Nav-Electronics.jpg', '2020-09-06 04:21:23', '2020-09-06 04:21:23', NULL),
(34602366, 'My Gas Station', 'Mohali 7 Phase, Sector 61, Sahibzada Ajit Singh Nagar, India', '30.7017355', '76.7247759', '1599487780259action-asphalt-auto-automobile-303316.jpg', '2020-09-07 14:09:40', '2020-09-07 14:09:40', NULL),
(34602367, 'Booking in Mohali', 'Mohali 7 Phase, Sector 61, Sahibzada Ajit Singh Nagar, India', '30.7017355', '76.7247759', '1599488016790balance-macro-ocean-pebbles-235990.jpg', '2020-09-07 14:13:36', '2020-09-07 14:13:36', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `firstName` varchar(60) DEFAULT '',
  `lastName` varchar(60) DEFAULT '',
  `email` varchar(60) DEFAULT '',
  `phoneNumber` varchar(30) NOT NULL,
  `countryCode` varchar(20) NOT NULL,
  `password` varchar(100) NOT NULL DEFAULT '',
  `address` varchar(500) DEFAULT NULL,
  `image` text DEFAULT NULL,
  `deviceToken` varchar(255) NOT NULL DEFAULT '',
  `userType` varchar(255) NOT NULL DEFAULT '1',
  `sessionToken` varchar(500) NOT NULL DEFAULT '',
  `moduleType` varchar(255) NOT NULL DEFAULT '',
  `platform` varchar(255) NOT NULL DEFAULT '',
  `status` int(11) NOT NULL DEFAULT 1,
  `createdAt` datetime NOT NULL DEFAULT '2020-05-28 06:48:04',
  `updatedAt` datetime NOT NULL DEFAULT '2020-05-28 06:48:04'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `firstName`, `lastName`, `email`, `phoneNumber`, `countryCode`, `password`, `address`, `image`, `deviceToken`, `userType`, `sessionToken`, `moduleType`, `platform`, `status`, `createdAt`, `updatedAt`) VALUES
('bf4a262b-22b2-4363-95d8-5d1fc7c3748a', 'Newuser', '', 'newuser@gmail.com', '99141993345', '+1', '$2a$10$79QYPEfSArT0byTz1d4JA.b.3Ts0Xo1BqDrZIJbOUpqyF9TNrSb2y', '', '', '', '1', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1aWQiOiJiZjRhMjYyYi0yMmIyLTQzNjMtOTVkOC01ZDFmYzdjMzc0OGEiLCJpYXQiOjE1OTk0OTA0NDEsImV4cCI6MTU5OTQ5MDc0MX0.dMtMEBu2_Fx5HMbgto-L7TZgO-2YDCQjD30u3IfkHGU', '', 'ios', 1, '2020-04-20 00:00:00', '0000-00-00 00:00:00'),
('eaacc3db-a139-4a45-9b93-1559f7a88298', 'Naval', 'uppal', 'sad@gmail.com', '7973015382', '+91', '', 'sad', '1591795468613_', '', '1', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwaG9uZU51bWJlciI6Ijc5NzMwMTUzODIiLCJteUNvbXBhbnlJZCI6IjI1Y2JmNThiLTQ2YmEtNGJhMi1iMjVkLThmOGY2NTNlOWYxMSIsImNvdW50cnlDb2RlIjoiKzkxIiwidXNlclR5cGUiOjEsInBhcmVudENvbXBhbnkiOiIyNWNiZjU4Yi00NmJhLTRiYTItYjI1ZC04ZjhmNjUzZTlmMTEiLCJpZCI6ImVhYWNjM2RiLWExMzktNGE0NS05YjkzLTE1NTlmN2E4ODI5OCIsImlhdCI6MTU5MjE5NDUzMCwiZXhwIjoxNTkyMzY3MzMwfQ.bZX8zpRnVizV5lQCGvilGojb-mU_DKVF4jlAYE_6oxs', '', 'ios', 1, '2020-05-24 08:39:29', '2020-05-24 08:39:29');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `booking`
--
ALTER TABLE `booking`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pump`
--
ALTER TABLE `pump`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `booking`
--
ALTER TABLE `booking`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=90;

--
-- AUTO_INCREMENT for table `pump`
--
ALTER TABLE `pump`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34602368;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
